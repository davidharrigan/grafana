import configparser
import os
import socket
import sys
import time

import requests
from telegraf.client import TelegrafClient


class Config:

    def __init__(self):
        config_file = os.path.join(os.getcwd(), 'config.ini')
        if not os.path.isfile(config_file):
            print('Unable to read config.ini')
            sys.exit(1)
        config = configparser.ConfigParser()
        config.read(config_file)
        self._config = config
        self.load()

    def load(self):
        self.delay = self._config['general'].getint('delay', fallback=300)
        self.error_backoff = self._config['general'].getint('consecutive_error_backoff', 0)

        self.host = self._config['telegraf'].get('host', fallback='localhost')
        self.port = self._config['telegraf'].getint('port', fallback=8092)
        self.database = self._config['telegraf'].get('database', fallback='telegraf')

        self.tags = {}
        for k, v in self._config['tags'].items():
            self.tags[k] = v
        if 'host' not in self.tags:
            self.tags['host'] = socket.gethostname()


def check_pia():
    url = 'https://www.privateinternetaccess.com/pages/whats-my-ip/'
    res = requests.get(url)
    if "You are protected by PIA" in res.text:
        return True
    return False


# TODO: use logging
def main():
    config = Config()
    client = TelegrafClient(
        host=config.host,
        port=config.port,
        tags=config.tags)

    consecutive_errors = 0
    while True:
        connected = 0
        try:
            if check_pia():
                connected = 1
            consecutive_errors = 0
        except Exception:
            if config.error_backoff > 0:
                consecutive_errors += 1
                if consecutive_errors > config.error_backoff:
                    print('consecutive errors exceeded')
                    sys.exit(1)
        print('connected=', connected)
        client.metric('pia_status', {'connected': connected})
        time.sleep(config.delay)


if __name__ == '__main__':
    main()
